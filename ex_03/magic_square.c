int	magic_square(int *sqr)
{
  //if (sqr[0] + sqr[1] + sqr[2] == sqr[3] + sqr[4] + sqr[5] &&
  //  sqr[0] + sqr[1] + sqr[2] == sqr[3] + sqr[4] + sqr[5] &&
  int	i = 0;
  int	val = sqr[0] + sqr[1] + sqr[2];
  int	start = 3;
  int	decal = 1;
  int	incr = 3;
  while (i < 7)
    {
      int	cmp = 0;
      int	j = 0;

      while (j < 3)
	{
	  cmp = cmp + sqr[start + decal * j];
	  ++j;	    
	}
      if (cmp != val)
	return (1);
      start = start + incr;
      if (i == 1)
	{
	  decal = 3;
	  start = 0;
	  incr = 1;
	}
      else if (i == 4)
	{
	  start = 0;
	  decal = 4;
	}
      else if (i == 5)
	{
	  start = 2;
	  decal = 2;
	}
      ++i;
    }
  return (0);
}
