int	matrices_addition(int **mat, int column_offset, int row_offset, int size, int direction)
{
  int	i;
  int	j;
  int	res;
  int	x;
  int	y;
  
  res = 0;
  y = 0;
  x = 0;
  if (direction > 12 || direction < 0)
    return (0);
  if (direction > 0 && direction < 6)
    x = 1;
  else if (direction > 6 && direction < 12)
    x = -1;
  if (direction < 3 || direction > 9)
    y = -1;
  else if (direction < 9 && direction > 3)
    y = 1;
  for (i = column_offset, j = row_offset; i < size && j < size && i >= 0 && j >= 0;)
    {
      res = res + mat[j][i];
      i = i + x;
      j = j + y;
    }
  return (res);
}
