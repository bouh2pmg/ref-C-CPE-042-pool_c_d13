#include <stdlib.h>

int	*merge_array(int *arr1, int *arr2, int size1, int size2)
{
  int	i;
  int	*ret;

  if (!(ret = malloc(sizeof(*ret) * (size1 + size2))))
    return (NULL);
  i = 0;
  while (i < size1)
    {
      ret[i] = arr1[i];
      ++i;
    }
  i = 0;
  while (i < size2)
    {
      ret[i + size1] = arr2[i];
      ++i;
    }
  return (ret);
}
