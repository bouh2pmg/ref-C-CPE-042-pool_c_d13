#include <stdlib.h>
#include <stdio.h>

# define LEFT "left"
# define RIGHT "right"
# define AHEAD "ahead"
# define BACK "back"
# define RABBIT "RABBIT !!!"


int	follow_the_white_rabbit()
{
  int	dice;
  int	res;
  char	*to_print = NULL;
  
  res = 0;
  while (res < 397)
    {
      to_print = NULL;
      dice = 1 + random() % 37;
      res = res + dice;
      switch (dice)
	{
	case 1:
	  to_print = RABBIT;
	  break;
	case 4:
	case 5:
	case 6:
	  to_print = LEFT;
	  break;
	case 8:
	  to_print = BACK;
	  break;
	case 10:
	  to_print = AHEAD;
	  break;
	case 13:
	  to_print = RIGHT;
	case 15:
	  to_print = AHEAD;
	  break;
	case 16:
	  to_print = BACK;
	  break;
	case 17:
	  to_print = LEFT;
	  break;
	case 18:
	case 19:
	case 20:
	case 21:
	  to_print = LEFT;
	  break;
	case 23:
	  to_print = AHEAD;
	  break;
	case 24:
	case 25:
	case 26:
	case 27:
	  to_print = BACK;
	  break;
	case 28:
	  to_print = LEFT;
	  break;
	case 29:
	case 30:
	case 31:
	case 32:
	case 33:
	  to_print = BACK;
	  break;
	case 34:
	case 35:
	case 36:
	  to_print = RIGHT;
	  break;
	case 37:
	  to_print = RABBIT;
	  break;
	default:
	  break;
	}
      if (to_print)
	{
	  printf("%s\n", to_print);
	  if (to_print == RABBIT)
	    return (res);
	}
    }
  printf("%s\n", RABBIT);
  return (res);
}
