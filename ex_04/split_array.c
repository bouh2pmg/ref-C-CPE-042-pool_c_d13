#include <stdlib.h>

int	**split_array(int *arr, int size, int *new_size1, int *new_size2)
{
  int	i;
  int	evens = 0;
  int	odds = 0;
  int	**ret;
  
  i = 0;
  while (i < size)
    {
      if (arr[i] % 2 == 0)
	++evens;
      else
	++odds;
      ++i;
    }
  i = 0;
  ret = malloc(sizeof(*ret) * 2);
  ret[0] = malloc(sizeof(**ret) * (odds));
  ret[1] = malloc(sizeof(**ret) * (evens));
  *new_size1 = odds;
  *new_size2 = evens;
  int j = 0, k = 0;
  while (i < size)
    {
      if (arr[i] % 2 == 0)
	ret[1][j++] = arr[i];
      else
	ret[0][k++] = arr[i];
      ++i;
    }
  return (ret);
}
